# Amethyst

*A general-purpose keyboard-centric launcher with a radial menu.*

Please read the [Amethyst documentation](https://roipoussiere.frama.io/amethyst) for more information about Amethyst, how to use it and how to contribute.
