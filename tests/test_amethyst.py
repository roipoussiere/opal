"""Main unit test suite for the Amethyst project."""

from amethyst import __version__


def test_version():
    """Returns the current version of the Amethyst project."""
    assert __version__ == '0.1.0'
