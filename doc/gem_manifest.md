# Gem manifest

The gem manifest describes a gem and its faces. It can be stored in yaml or json.

<!--
## Title (`title`)

A short string describing the gem, used by parent module and displayed on the home face when hint mode is enabled.

```yml
title: Awesome module
```

## Icon (`icon`)

The gem icon, used by parent module and displayed on the home face.

By default, this is [material design icon](https://materialdesignicons.com/) name (included in Amethyst), but other resource types can be provided by using suffixes: 
:
- `icon.url`: Amethyst will load an icon remotely (useful for web-based modules);
- `icon.base64`: a png icon image encoded in base64;
- `icon.svg`: a vector image in svg;
- `icon.xdg`: an xdg icon;
- `icon.chars`: 1 or 2 characters, such as `Rt` (avoid emojis here, that are system-dependant, use icons instead).

If no icon is provided, Amethyst use `icon.chars` with the 2 first characters of the module name.

```yml
icon: 'heart'
```

## Meta (`meta`)

Meta information about the module:

- repository url: The repository url where the module code is hosted;
- maintainer: name and email of the maintainer, can be an object with `name` and `mail` keys, or a string in the form `Some Name <mail@example.com>`;
- dependencies: eventual dependencies required for this module (informationnal).

```yml
meta:
  repository_url: https://gitlab.com/foo/bar
  maintainer:
    name: John Doe
    mail: john-doe@example.com
  dependencies: [foo, bar] # informationnal
```
-->

## Faces (`faces`)

The list of faces to display.

```yaml
faces:
  north:
    label: up
    icon: arrow-up
  south:
    label: down
    icon: arrow-down
```

Face items must be one of: `north`, `north-east`, `east`, `south-east`, `south`, `south-west`, `west`, or `north-west`.

Faces attributes are described below.

### Face label (`label`)

The label of the face, displayed when hint mode is enabled.

### Face icon (`icon`)

The face icon (see [gem icon](#icon-icon) for details).

<!--
### Face type (`type`)

- **standard face**: exit Amethyst and execute a specific action (default);
- **static face**: does not exit Amethyst, which can be useful to let user select a number for instance;
- **web face**: open a web page on the user's web browser;
- **module face**: open an other Amethyst module - by convetion, submodules are named with dot-seperated names, like in `module.submodule`;
- **navigation face**: change current faces without going to a deeper module level (useful to display many faces on several "pages");
- **inactive**: a face that do nothing, that can be used to let the user understand that this action can be done, but not in the current context (ie. a right arrow that increase a value which is already at its maximum).
-->

<!-- To be decided: **information**: Open a window with long text inside, with a single face slot available. -->
