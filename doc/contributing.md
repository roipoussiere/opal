# Contributing to Amethyst

Thank you for looking at this page! <3

There are many ways to contribute to Amethyst.

## Make this project known

- add a star on [the Gitlab repository](https://framagit.org/roipoussiere/amethyst);
- share this project on you favorite social media;
- talk about Amethyst to your family and friends.

## Provide feedback

Want to report a bug or suggest a feature? Just [create a new issue](https://framagit.org/roipoussiere/amethyst/issues/new) on the Gitllab repository.

If you are not used to Gitlab, you can also [contact me on Mastodon](https://mastodon.tetaneutral.net/@roipoussiere).

I'm concerned about accessibility: if you have some disabilities such as physical disability or blindness, please let me know how Amethyst can help you in some way and how to improve it.

## Be creative

### Create new color palettes

Amethyst's colors palettes includes 16 different colors (just like the very common [ANSI 4 bit colors](https://en.wikipedia.org/wiki/ANSI_escape_code#Colors), used on many text editors).

Read the [color palette creation guide](./creating_color_palettes.md) for more details.

### Create new modules

Amethyst's modules are yaml or json files that describe the content of the Amethyst UI.

### Create new plugins

Plugins creation requires some programming skills in Python. You first need to [set up a development project](#contribute-to-code).

### Share your work

Just created a handy module? Share it around you!

Also, don't hesitate to [create an issue](#provide-feedback) if you want to include it in project built-in works, in this way any Amethyst user can enjoy it easily.

## Update documentation

Amethyst documentation is built with [MkDocs](https://www.mkdocs.org/) using the [ReadTheDocs](https://www.mkdocs.org/user-guide/styling-your-docs/#readthedocs) theme.
See the `mkdocs.yml` config file for details about its configuration.

### Small modification

If you just want to fix a typo, you can use the GitLab built-in interface to directly edit a page and create a pull request in order to let me include your modification in the next version of Amethyst.

Just select a file from [a documentation page on Gitlab](https://framagit.org/roipoussiere/amethyst/-/tree/master/doc) and click on the *Edit* button.
You must have an account on [Framagit](https://framagit.org/).

### Bigger modification

You may want edit documentation locally.

First download and fork the project (read the [Contribute to code](#contribute-to-code) section below), then edit markdown files in the `doc` folder.

Then you can build the documentation and serve it locally with:

```bash
make serve-docs
```

Then go to `http://127.0.0.1:8000/` with your favorite web browser to access to the documentation.

## Contribute to code

### Install Poetry

[Poetry](https://python-poetry.org/) is used to resolve dependencies, as well as to create and publish package and must be [installed](https://python-poetry.org/docs/#installation).
Using Poetry is not mandatory but encouraged since instructions below assumes that you use this tool.

### Install Amethyst for development

```
git clone https://framagit.org/roipoussiere/amethyst.git
cd amethyst
poetry install
```

And if you want to work on the Qt version of Amethyst:

```
poetry install --extras qt
```

### Run Amethyst development version

Use the `poetry run amy` command the run Amethyst in the Poetry virtual environment, for instance:

```
echo -e 'foo\nbar\nbaz' | poetry run amy -d -
```

## Developing

### Fork the project

1. [Fork the Amethys project](https://framagit.org/roipoussiere/amethyst/-/forks/new);
2. update the url of the *origin* remote (you can get it by clicking the *clone* button on your fork page on GitLab):

```
git remote set-url origin https://framagit.org/<your_gitlab_user>/amethyst.git
```

### Work with Python 3.6

Amethyst is supported starting from Python 3.6, thus you should work with this Python version.

So first, install it if you don't have already, among with the venv tool. For instance on Ubuntu 20.04 it's:

```
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install python3.6 python3.6-venv
```

Then tell Poetry to use this Python version:

```
poetry env use 3.6
```

### Set Python interpreter

You may select the Python interpreter used by Poetry in your IDE, you can find it with:

```
ls $(poetry config settings.virtualenvs.path | tr -d '"')/amethyst-*/bin/python3
```

### Test your code

Before sending a merger request, you should analyse project code with [Pylint](https://www.pylint.org/):

```bash
make lint
```

Note that this test is executed by the GitLab continuous integration.

### Create a merge request

Create a new git branch, write code, create commits, push to your fork and [create a merge request](https://framagit.org/roipoussiere/amethyst/-/merge_requests/new)!
