# Amethyst

<img style="display: block; margin: auto;" src="img/amethyst_logo.png">

Amethyst is a general-purpose keyboard-centric launcher with a radial menu.

!!! Important
    This project is under development. Don't really expect things to work for now.

Amethyst displays a user interface (called gem) composed of several choices (called faces), then executes an action accordingly to what has been selected.
Gems are described by a *manifest*, that gives information about it and its faces (labels, position, icon, etc.).

Amethyst works with modules, which are executable files providing the gem manifest and actions to do.

Modules can be built-in, provided by the community, or hand crafted by users like you.

You can also configure the user interface by editing the configuration file, and define a color palette (that describes the colors used in the Amethyst UI).

## Limited user interface

The key point of the user interface is that it is intentionally very limited:

- no mouse support;
- maximum height items to select;
- no long text allowed;
- modules can not take action on the look & feel.

This limitation have some side effects:
- modules developers must create concise and simple gems;
- users discovers available actions and executes tasks quickly.
