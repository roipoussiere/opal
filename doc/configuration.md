# Configuration

<!-- Amethyst can be configured both by editing its configuration file or from Opal UI (see [Builtin module: configuration](Builtin-modules#configuration)) -->

<!-- Each configuration variable can be passed via the `--config` CLI argument. In this case the value overrides the value defined in the configuration file. `--config` notation uses dot-separated levels and comma-separated values, like `--config style.background.color=#202020,default.browser=firefox`. -->

<!-- Amethyst config file is formatted in Yaml and stored in `~/.config/opal/config.yml`. A sample configuration is created when installing Opal. -->

## Attributes

### Width (`width`)

Width of the Amethyst ui container, which is also its height since this container is a square.
